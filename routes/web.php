<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('sendSMS', 'MainController@processLeadSMS')->name('sendSMS');
Route::post('sendRequestToClickatell', 'MainController@sendRequestToClickatell')->name('sendRequestToClickatell');
