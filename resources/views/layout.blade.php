<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta name="viewport" content="width=device-width, initial-scale=0.1">
    <title>Clickatell SMS - @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Style sheets-->

    <link href="{{ URL::asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/SpinKit/spinkit.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/jquery.steps/jquery.steps.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/amanda.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/timepicker/jquery.timepicker.css') }}" rel="stylesheet">

    <!--Scripts-->
    <script type="text/javascript" src="{{ URL::asset('assets/jquery/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/popper.js/popper.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/bootstrap/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/jquery-ui/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/jquery-toggles/toggles.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/jquery.steps/jquery.steps.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/timepicker/jquery.timepicker.js') }}"></script>

    <!-- For JQuery Tabs-->
    <script type="text/javascript" src="{{ URL::asset('assets/js/amanda.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/lib/handlebars.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/lib/ZohoEmbededAppSDK.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/lib/customFunction.js') }}"></script>
    <!-- Moments-->
    <script type="text/javascript" src="{{ URL::asset('assets/lib/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/lib/moment/moment-timezone-with-data.min.js') }}"></script>
    <style type="text/css">
        #wizard .content {
            background: none !important;
        }
    </style>
</head>

<body>
    <!-- ############ PAGE START-->
    @yield('content')
    <!-- ############ LAYOUT END-->
    <script type="text/javascript">
        var htmlModalBody = '<strong>On Processing. Please wait...</strong>';
        htmlModalBody = htmlModalBody + '<div class="progress mb-2">';
        htmlModalBody = htmlModalBody + '<div class="progress-bar progress-bar-striped progress-bar-animated primary" style="width: 100%">&nbsp;</div>';
        htmlModalBody = htmlModalBody + '</div>';
    </script>
    <!-- endbuild -->
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @yield('customJavascript')
</body>

</html>