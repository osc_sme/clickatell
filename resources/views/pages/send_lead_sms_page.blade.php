@extends('layout')
@section('page-title', 'Send SMS')
@section('customJavascript')
<script type="text/javascript">
    $(document).ready(function() {
        var urlOrigin = window.location.origin;
        var baseUrl = (urlOrigin == 'https://localhost') ? urlOrigin + '/clickatell/public' : urlOrigin + '/clickatell/public';
        // var baseUrl = urlOrigin;

        setTimeout(function() {
            $('.processing').hide();
            $('#sendSMSUsingCustomBody').slideDown('slow');
        }, 3000);
        $("div#smsTemplateDiv").hide();

        var clickatellAuthToken;
        var incomingMSGUrl;
        var statusBackUrl;
        var processedSMSTesmplates = new Array();
        var CRMTimeZone;
        var allUsers = new Array();

        ZOHO.embeddedApp.on("PageLoad", function(data) {
            recordId = data.EntityId;
            mobuleName = data.Entity;
            processedModuleName = mobuleName.substr(0, (mobuleName.length - 1));

            ZOHO.CRM.UI.Resize({
                height: "475",
                width: "700"
            }).then(function(data) {
                //console.log(data);
            });

            //Load all SMS Templates
            ZOHO.CRM.API.searchRecord({
                Entity: "clickatellsms__SMS_Templates",
                Type: "criteria",
                Query: "(clickatellsms__Module_Name:equals:" + mobuleName + ")"
            }).then(function(allSMSTemplates) {
                var smsTemplates = allSMSTemplates.data;

                var templasteOptions = '<option value="">Select Template</option>';
                $.each(smsTemplates, function(stIndex, templateInfo) {

                    var sTemplate = {};
                    $.each(templateInfo, function(fieldName, fieldValue) {
                        if (fieldName == "id") {
                            sTemplate[fieldName] = fieldValue;
                            templasteOptions = templasteOptions + '<option value="' + fieldValue + '">' + templateInfo.Name + '</option>';
                        } else if (fieldName == "clickatellsms__Template_Body") {
                            sTemplate['Template_Body'] = fieldValue;
                        }
                    });
                    processedSMSTesmplates.push(sTemplate);
                });
                // console.log(templasteOptions);
                $("#smsTemplate").html(templasteOptions);
            });

            //Load user merge fields
            var userMergeFields = '<option value="">Select User Field</option>';
            ZOHO.CRM.META.getFields({
                "Entity": "Users"
            }).then(function(data) {
                var userFiedls = data.fields;
                for (i = 0; i < userFiedls.length; i++) {
                    userFieldInfo = userFiedls[i];
                    fieldApiName = userFieldInfo.api_name;
                    fieldDisplayName = userFieldInfo.field_label;
                    if (fieldApiName != "Modified_By" && fieldApiName != "created_time" && fieldApiName != "Modified_Time" && fieldApiName != "locale" && fieldApiName != "created_by" && fieldApiName != "time_format" && fieldApiName != "time_zone" && fieldApiName != "country_locale") {
                        userMergeFields = userMergeFields + '<option value="${Users.' + fieldApiName + '}">' + fieldDisplayName + '</option>';
                    }
                }
                $("#userMergeFields").html(userMergeFields);
            });

            //Load Module fields
            ZOHO.CRM.META.getLayouts({
                "Entity": mobuleName
            }).then(function(data) {
                var sectionName;
                var fieldOptions;
                var sectionIndexOf;
                var sectionListed = new Array();
                var mergeFields = '<option value="">Select ' + processedModuleName + ' Field</option>';
                $.each(data.layouts, function(layoutKey, layoutInfo) {
                    $.each(layoutInfo.sections, function(section_key, sectionInfo) {
                        if (sectionInfo.fields.length > 0) {
                            sectionName = sectionInfo.name;
                            sectionIndexOf = sectionListed.indexOf(sectionName);
                            if (sectionIndexOf == "-1") {
                                fieldOptions = processOptions(data.layouts, sectionName, mobuleName);
                                mergeFields = mergeFields + ' <optgroup label="' + sectionName + '">' + fieldOptions + '</optgroup>';
                                sectionListed.push(sectionName);
                            }
                        }
                    });
                });
                $("#mergeFields").html(mergeFields);
            });

            //Get All users & process
            ZOHO.CRM.API.getAllUsers({
                Type: "ActiveUsers"
            }).then(function(data) {
                var users = data.users;
                for (i = 0; i < users.length; i++) {
                    ZOHO.CRM.API.getUser({
                            ID: users[i].id
                        })
                        .then(function(userData) {
                            var userInfo = userData.users[0];
                            allUsers.push(userInfo);
                        });
                }
            });

            //get login user Information & Timezone
            ZOHO.CRM.CONFIG.getCurrentUser().then(function(data) {
                loginUserId = data.users[0].id;
                ZOHO.CRM.API.getUser({
                    ID: loginUserId
                }).then(function(user_data) {
                    currentUserTimeZone = user_data.users[0].time_zone;
                    if (currentUserTimeZone == "") {
                        $(".zoneAlert").slideDown('slow');
                    }
                });
            });
        });

        ZOHO.embeddedApp.init().then(function() {
            //Read Org variables
            ZOHO.CRM.API.getOrgVariable("clickatellsms__ClickatellAuthToken").then(function(data) {
                var newOrg = (data.Success);
                clickatellAuthToken = newOrg.Content;
            });

            //incoming sms url
            ZOHO.CRM.API.getOrgVariable("clickatellsms__IncomingWebHookURL").then(function(data) {
                var newOrg = (data.Success);
                incomingMSGUrl = newOrg.Content;
            });

            //sms status url
            ZOHO.CRM.API.getOrgVariable("clickatellsms__SMSStatusCallbackUrl").then(function(data) {
                var newOrg = (data.Success);
                statusBackUrl = newOrg.Content;
            });

            ZOHO.CRM.CONFIG.getOrgInfo().then(function(data) {
                OrgCRMTimeZone = data.org[0].time_zone;

                //moment.tz.setDefault(CRMTimeZone);
            });
        });

        //sending sms start
        $('#wizard').steps({
            headerTag: 'h3',
            bodyTag: 'section',
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
            onStepChanging: function(event, currentIndex, newIndex) {
                $(".alert-warning").hide();
                if (currentIndex < newIndex) {
                    // Step 1 form validation
                    if (currentIndex === 0) {
                        $(".alert-warning").hide();
                        var smsOptionVal = $("#smsOption").val();

                        if (smsOptionVal == "") {
                            $('.btn-warning').css({
                                "border": "1px solid red"
                            });
                            $("#errorContent").html('<strong class="d-block d-sm-inline-block-force">Choose SMS Body!</strong> In order to send SMS you must need to type text or select SMS template.');
                            $(".errorMsg").slideDown('slow');
                            setTimeout(function() {
                                $('.errorMsg').slideUp('slow');
                            }, 3000);
                            $('.btn-warning').focus();
                            return false;
                        }

                        if (smsOptionVal == "template") {
                            var validTrue = $("#smsTemplate").val();
                            if (validTrue != "") {
                                return true;
                            } else {
                                $("#errorContent").html('<strong class="d-block d-sm-inline-block-force">SMS Template!</strong> In order to send SMS you must need to select SMS template.');
                                $(".errorMsg").slideDown('slow');
                                $("#smsTemplate").focus();
                                $("#smsTemplate").css({
                                    "border": "1px solid #dc3545"
                                });
                                setTimeout(function() {
                                    $('.errorMsg').slideUp('slow');
                                }, 3000);
                                return false;
                            }
                        } else if (smsOptionVal == "customBody") {
                            var validTrue = $("#customSMSBody").val();
                            if (validTrue != "") {
                                return true;
                            } else {
                                $("#errorContent").html('<strong class="d-block d-sm-inline-block-force">Custom SMS Body!</strong> In order to send SMS you must need to type text.');
                                $(".errorMsg").slideDown('slow');
                                $("#customSMSBody").focus();
                                $("#customSMSBody").css({
                                    "border": "1px solid #dc3545"
                                });
                                setTimeout(function() {
                                    $('.errorMsg').slideUp('slow');
                                }, 3000);
                                return false;
                            }
                        }
                    }

                    // Step 2 form validation
                    if (currentIndex === 1) {
                        $("#processedSMSBody").html($("#customSMSBody").val());
                        var sendOptionVal = $("#sendOption").val();

                        if (sendOptionVal == "") {
                            $('.btn-warning').css({
                                "border": "1px solid red"
                            });
                            $("#errorContent").html('<strong class="d-block d-sm-inline-block-force">Sending Option!</strong> In order to send SMS you must need to choose sending option.');
                            $(".errorMsg").slideDown('slow');
                            $('.btn-warning').focus();
                            setTimeout(function() {
                                $('.errorMsg').slideUp('slow');
                            }, 3000);
                            return false;
                        }

                        if (sendOptionVal == "schedule") {
                            var scheduleTimeDate = $("#scheduleTimeDate").val();
                            var scheduleTimeTime = $('#scheduleTimeTime').val();

                            if (scheduleTimeDate != "" && scheduleTimeTime != "") {
                                var scheduleTimeTimeExp = scheduleTimeTime.split(" ");
                                var scheduleTimeTimeExpF = scheduleTimeTimeExp[0].split(":");

                                if ((typeof(scheduleTimeTimeExp[1]) == "undefined" && scheduleTimeTimeExp[1] != "AM" && scheduleTimeTimeExp[1] != "PM") && (typeof(scheduleTimeTimeExpF[1]) == "undefined" && scheduleTimeTimeExpF[1] > 59 && scheduleTimeTimeExpF[0]) > 12) {
                                    $("#errorContent").html('<strong class="d-block d-sm-inline-block-force">Schedule Time!</strong> Your given schedule time is invalid. As example 09/09/1970 03:15 PM.');
                                    $(".errorMsg").slideDown('slow');
                                    $("#scheduleTimeTime").focus();
                                    $("#scheduleTimeTime").css({
                                        "border": "1px solid #dc3545"
                                    });
                                    setTimeout(function() {
                                        $('.errorMsg').slideUp('slow');
                                    }, 3000);
                                    return false;
                                } else {
                                    $(".alert-warning").show();
                                    $("#scheduleTimeDate").css({
                                        "border": "1px solid rgba(0, 0, 0, 0.15)"
                                    });
                                    return true;
                                }
                            } else {
                                $("#errorContent").html('<strong class="d-block d-sm-inline-block-force">Schedule Time!</strong> In order to send schedule SMS you need to select schedule time.');
                                $(".errorMsg").slideDown('slow');
                                setTimeout(function() {
                                    $('.errorMsg').slideUp('slow');
                                }, 3000);
                                $("#scheduleTimeDate").focus();
                                $("#scheduleTimeDate").css({
                                    "border-left": "1px solid #dc3545",
                                    "border-top": "1px solid #dc3545",
                                    "border-bottom": "1px solid #dc3545"
                                });
                                $("#scheduleTimeTime").css({
                                    "border-right": "1px solid #dc3545",
                                    "border-top": "1px solid #dc3545",
                                    "border-bottom": "1px solid #dc3545"
                                });
                                return false;
                            }
                        } else {
                            $(".alert-warning").show();
                            return true;
                        }
                    }
                    // Always allow step back to the previous step even if the current step is not valid.
                } else {
                    return true;
                }
            },
            onFinishing: function(event, currentIndex) {
                var SMSBody = $("#customSMSBody").val();
                var smsTemplateId = $("#smsTemplate").val();
                var scheduleTimeDate = $("#scheduleTimeDate").val();
                var scheduleTimeTime = $("#scheduleTimeTime").val();
                var token = $('input[name="_token"]').val();
                var recordInfo, recordOwnerID, recipientMobile, recordModifiedBy, smsSendBy, SMSOwnerId, sendSMSName, noteContent;

                $("#processing").slideDown("slow");

                ZOHO.CRM.API.getRecord({
                    Entity: mobuleName,
                    RecordID: recordId
                }).then(function(recordDetail) {

                    recordId = recordId[0];
                    recordInfo = recordDetail.data[0];
                    recordOwnerID = recordInfo.Owner.id;

                    recipientMobile = '+' + recordInfo.Mobile;
                    if (recipientMobile == null || recipientMobile == "") {
                        recipientMobile = '+' + recordInfo.Phone;
                    }
                    recordModifiedBy = loginUserId;
                    smsSendBy = recordModifiedBy;

                    //By default sms is sending by crm login user
                    SMSOwnerId = smsSendBy;

                    if (currentUserTimeZone != "") {
                        CRMTimeZone = currentUserTimeZone
                    } else {
                        CRMTimeZone = orgCRMTimeZone;
                    }

                    //Merge user fields
                    SMSBody = mergeUserFields(SMSBody, allUsers, smsSendBy);


                    //Merge module fields
                    SMSBody = mergeRecordFields(SMSBody, recordInfo, mobuleName, CRMTimeZone);

                    var sendSMSData = {};
                    sendSMSData['clickatellsms__' + processedModuleName + '_Name'] = recordId;
                    sendSMSData['Owner'] = SMSOwnerId;
                    sendSMSData['clickatellsms__Custom_SMS_Body'] = SMSBody;
                    sendSMSData['clickatellsms__SMS_Characters_Total'] = SMSBody.length;
                    sendSMSData['clickatellsms__Template'] = smsTemplateId;
                    sendSMSData['clickatellsms__Type'] = 'Outbound';

                    var currentDateTime = moment().tz(CRMTimeZone).format('DD/MM/YYYY hh:mm:ss A');
                    if (smsTemplateId != "") {
                        sendSMSName = "Template Based SMS on " + currentDateTime;
                    } else {
                        sendSMSName = "Custom Body SMS on " + currentDateTime;
                    }

                    if (scheduleTimeDate != "") {

                        var processDatePart = scheduleTimeDate.split('/');
                        var datePartAfterProcess = processDatePart[2] + '-' + processDatePart[1] + '-' + processDatePart[0];
                        var fprocessscheduleTime = datePartAfterProcess + ' ' + scheduleTimeTime;
                        var userTimeSheduleTime = moment(fprocessscheduleTime, "YYYY-MM-DD HH:mm:ss A").tz(CRMTimeZone);
                        var TimeFormat = moment(fprocessscheduleTime, "YYYY-MM-DD hh:mm:ss A");

                        var newscheduleTimeTime = moment(scheduleTimeTime, ["h:mm A"]).format("HH:mm");
                        var newprocessscheduleTime = datePartAfterProcess + ' ' + newscheduleTimeTime;

                        var UnixEpochTime = moment(userTimeSheduleTime).unix();

                        var dateTime = new Date(newprocessscheduleTime);
                        var newdateTime = moment(dateTime).format("DD/MM/YYYY hh:mm:ss A");

                        noteContent = processedModuleName + ": The SMS is scheduled at " + newdateTime;

                        scheduleTimeInput = moment(TimeFormat).format();
                        //requestParams["send_at"] = scheduleTimeStamp;
                        sendSMSData['clickatellsms__Schedule_Time'] = scheduleTimeInput;

                        smsStatus = "Scheduled";

                        const Url = baseUrl + "/sendRequestToClickatell";
                        const Data = {
                            SMSBody: SMSBody,
                            recipientMobile: recipientMobile,
                            recordId: recordId,
                            recordInfo: recordInfo,
                            smsStatus: smsStatus
                        }

                        // optional parameters
                        const otherParam = {
                            headers: {
                                'content-type': 'application/json; charset=UTF-8',
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            body: Data,
                            method: "POST"
                        }

                        fetch(Url, otherParam).then(data => {
                            var fetchedData = JSON.stringify(data)
                            console.log(fetchedData);
                            return fetchedData;
                        }).then(res => {
                            json_decode(res);
                            console.log(res)
                        }).catch(error => console.log(error))


                        // headers: {
                        //         'content-type': 'application/json; charset=UTF-8',
                        //         'authorization': clickatellAuthToken,
                        //         'x-Version': 1,
                        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        //     },

                        // ZOHO.CRM.API.searchRecord({
                        //     Entity: "clickatellsms__Send_SMS",
                        //     Type: "criteria",
                        //     Query: "((clickatellsms__" + processedModuleName + "_Name:equals:" + recordId + ")and(clickatellsms__Status:equals:Scheduled))"
                        // }).then(function(logDataDetails) {
                        //     console.log("This is Logdatadetails");
                        //     console.log(logDataDetails);

                        // });
                    } else {
                        console.log("This is else estatement");
                        smsStatus = "Send";

                        $.ajax({
                            type: 'POST',
                            url: baseUrl + "/sendRequestToClickatell", // point to server-side PHP script
                            data: {
                                clickatellAuthToken: clickatellAuthToken,
                                SMSBody: SMSBody,
                                recipientMobile: recipientMobile,
                                recordId: recordId,
                                recordInfo: recordInfo,
                                smsStatus: smsStatus
                            },
                            dataType: 'JSON',
                            success: function(response) {
                                console.log(response);
                            }
                        });
                    }
                });
            },
            labels: {
                finish: '<i class="fa fa-send mg-r-10">Send SMS</i>'
            }
        });

        $(".btn-warning").on('click', function() {
            $(".btn-warning").css({
                "border": "none"
            });
            $(".form-control").css({
                "border": "1px solid rgba(0, 0, 0, 0.15)"
            });
            var relVal = $(this).attr('rel');

            if (relVal == "template" || relVal == "customBody") {
                $(".btn-warning[rel='template']").css({
                    "box-shadow": "none"
                });
                $(".btn-warning[rel='customBody']").css({
                    "box-shadow": "none"
                });

                $(".btn-warning[rel='template']").addClass('disabled');
                $(".btn-warning[rel='customBody']").addClass('disabled');

                $(".btn-warning[rel='template']").removeClass('active');
                $(".btn-warning[rel='customBody']").removeClass('active');
            } else if (relVal == "schedule" || relVal == "sendNow") {
                $(".btn-warning[rel='schedule']").css({
                    "box-shadow": "none"
                });
                $(".btn-warning[rel='sendNow']").css({
                    "box-shadow": "none"
                });

                $(".btn-warning[rel='schedule']").addClass('disabled');
                $(".btn-warning[rel='sendNow']").addClass('disabled');

                $(".btn-warning[rel='schedule']").removeClass('active');
                $(".btn-warning[rel='sendNow']").removeClass('active');
            }

            $(this).removeClass('disabled');
            $(this).addClass('active');
            $(this).css({
                "box-shadow": "0 0 0 3px rgba(255, 193, 7, 0.5)"
            });

            var smsOptionVal = $("#smsOption").val();

            if (smsOptionVal != relVal && (relVal == "template" || relVal == "customBody")) {
                $(".fieldProc").hide();
                $("#smsOption").val(relVal);
                $("#" + relVal).slideDown('slow');

                if (relVal == "customBody") {
                    $("#customSMSBody").val('');
                    $("#smsTemplate").val('');
                }
            }
            var sendOptionVal = $("#sendOption").val();
            if (sendOptionVal == "sendNow") {
                $("#scheduleTimeDate").val('');
                $("#scheduleTimeTime").val('');
            }

            if (sendOptionVal != relVal && (relVal == "schedule" || relVal == "sendNow")) {
                $(".schedule").hide();
                $("#sendOption").val(relVal);
                $("." + relVal).slideDown('slow');
                if (relVal == "sendNow") {
                    $('.actions li a[href="#next"]').trigger("click");
                }
            }
        });


        $("#smsTemplate").on('change', function() {
            if ($(this).val() != "") {
                $("#smsTemplate").css({
                    "border": "1px solid rgba(0, 0, 0, 0.15)"
                });
            } else {
                $("#smsTemplate").css({
                    "border": "1px solid #dc3545"
                });
            }
        });

        $("#customSMSBody").on('keyup', function() {
            if ($(this).val() != "") {
                $("#customSMSBody").css({
                    "border": "1px solid rgba(0, 0, 0, 0.15)"
                });
            } else {
                $("#customSMSBody").css({
                    "border": "1px solid #dc3545"
                });
            }
        });

        $('#scheduleTimeTime').timepicker({
            'timeFormat': 'h:i A',
            'step': 15
        });
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'dd/mm/yy'
        });

        //Replace cursor by merge fields
        $('#userMergeFields').on('change', function() {
            var cursorPos = $("#customSMSBody").prop('selectionStart');
            var smsBody = $("#customSMSBody").val();
            var textBefore = smsBody.substring(0, cursorPos);
            var textAfter = smsBody.substring(cursorPos, smsBody.length);
            $("#customSMSBody").val(textBefore + $(this).val() + textAfter);
        });

        //Replace cursor by merge fields
        $('#mergeFields').on('change', function() {
            var cursorPos = $("#customSMSBody").prop('selectionStart');
            var smsBody = $("#customSMSBody").val();
            var textBefore = smsBody.substring(0, cursorPos);
            var textAfter = smsBody.substring(cursorPos, smsBody.length);
            $("#customSMSBody").val(textBefore + $(this).val() + textAfter);
        });

        $("#submitCancel").click(function() {
            ZOHO.CRM.UI.Popup.closeReload();
        });

        $("#smsTemplate").on('change', function() {
            var pTemplateId;
            var templateId = $(this).val();
            $.each(processedSMSTesmplates, function(index, templateInfo) {
                pTemplateId = templateInfo.id;
                if (pTemplateId == templateId) {
                    $("#customSMSBody").val(templateInfo.Template_Body);
                }
            });
        });
    });
</script>
@endsection
@section('content')
<!-- {{ csrf_token() }} -->
<div class="row row-sm">
    <div class="col-xl-12 mg-xl-t-0">
        <div class="row row-xs success" style="display: none;">
            <div class="card col-xl-6 mg-xl-t-0" style="position: absolute;z-index:999999;">
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="d-block d-sm-inline-block-force">Well done!</strong> You have successfully sent SMS to the selected record(s).
                </div><!-- alert -->
                <div class="row row-xs zoneAlert" style="display: none;">
                    <div class="card col-xl-6 mg-xl-t-0" style="position: absolute;z-index:999999;">
                        <div class="alert alert-warning" role="alert" style="display: block;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="d-flex align-items-center justify-content-start" style="font-size: 13px;margin: -9px 0px;">
                                <i class="icon ion-alert-circled alert-icon tx-24 mg-t-5 mg-xs-t-0"></i>
                                <span><strong class="d-block d-sm-inline-block-force">Warning!</strong> Your time zone is not selected in zoho, hence it is using zoho organization time zone. If you want to schedule SMS in your timezone please update the time zone in zoho account.</span>
                            </div>
                        </div>
                    </div>
                </div><!-- alert -->
            </div>
        </div>
        <div class="row row-xs errorMsg" style="display: none;">
            <div class="card col-xl-6 mg-xl-t-0" style="position: absolute;z-index:999999;">
                <div class="alert alert-danger mg-b-0" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <span id="errorContent"></span>
                </div><!-- alert -->
            </div>
        </div>
        <div class="card pd-15 pd-sm-20 form-layout form-layout-5" id="sendSMSUsingCustomBody" style="display: none;">
            <p class="mg-b-10 tx-gray-400">To send an SMS, you need to either select the SMS template from the template library or; you can write your own custom SMS Body. You can also have the option to send SMS instantly or schedule it on a specific date and time. <br /> <i>Note: Timezone taken from your SMS user timezone settings.</i></p>
            <div id="wizard">
                <h3>Choose SMS Body</h3>
                <section class="pd-sm-10">
                    <div class="row row-xs">
                        <div class="col-sm-6 mg-xl-t-0">
                            <button class="btn btn-warning disabled btn-block mg-b-10" rel="template">Choose SMS Template</button>
                        </div>
                        <div class="col-sm-6 mg-xl-t-0 text-right">
                            <button class="btn btn-warning disabled btn-block mg-b-10" rel="customBody">Write Your Own</button>
                        </div>
                        <input type="hidden" id="smsOption" name="smsOption" value="">
                    </div>
                    <div class="row row-xs mg-t-10 fieldProc" id="template">
                        <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                            <select id="smsTemplate" class="form-control select2"></select>
                        </div>
                    </div>
                    <div class="row row-xs mg-t-10 fieldProc" id="customBody">
                        <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                            <div class="fieldMergeSection row" style="margin-bottom: 8px;">
                                <select class="form-control select2 col-sm-6" id="mergeFields"></select>
                                <select class="form-control select2 col-sm-5" id="userMergeFields"></select>
                            </div>
                            <textarea rows="7" class="form-control" name="customSMSBody" id="customSMSBody"></textarea>
                        </div>
                    </div>
                </section>
                <h3>Sending Option</h3>
                <section>
                    <div class="row row-xs">
                        <div class="col-sm-6 mg-xl-t-0">
                            <button class="btn btn-warning disabled btn-block mg-b-10" rel="schedule">Send Later</button>
                        </div>
                        <div class="col-sm-6 mg-xl-t-0 text-right">
                            <button class="btn btn-warning disabled btn-block mg-b-10" rel="sendNow">Send Now</button>
                        </div>
                        <input type="hidden" id="sendOption" name="sendOption" value="">
                    </div>
                    <p class="schedule">In order to send later you need to select <b>Schedule Time</b>.</p>
                    <div class="row row-xs schedule">
                        <div class="col-sm-12 mg-t-9 mg-sm-t-0 input-group">
                            <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                            <input type="text" name="scheduleTimeDate" id="scheduleTimeDate" class="form-control fc-datepicker" placeholder="dd/mm/yyyy">
                            <input type="text" name="scheduleTimeTime" id="scheduleTimeTime" class="form-control" style="border-left: none;" placeholder="12:00 PM">
                        </div>
                    </div><!-- row -->
                </section>
                <h3>Send SMS for Lead</h3>
                <div class="alert alert-warning" role="alert" style="margin-bottom:6px;">
                    Review your SMS body and then send
                </div><!-- alert -->
                <section>
                    <p>The following SMS will send to the selected record(s).</p>
                    <p id="processedSMSBody"></p>
                </section>
            </div>
        </div><!-- card -->
        <div id="processing">
            <div class="d-flex ht-300 pos-relative align-items-center">
                <div class="sk-folding-cube">
                    <div class="sk-cube1 sk-cube"></div>
                    <div class="sk-cube2 sk-cube"></div>
                    <div class="sk-cube4 sk-cube"></div>
                    <div class="sk-cube3 sk-cube"></div>
                </div>
            </div><!-- d-flex -->
        </div>
    </div><!-- col-6 -->
    <div class="col-md-6 col-xl-4 mg-t-30 mg-xl-t-0 processing">
        <div class="d-flex ht-300 pos-relative align-items-center">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
            </div>
        </div><!-- d-flex -->
    </div><!-- col-4 -->
</div><!-- row -->
@endsection