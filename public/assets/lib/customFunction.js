function mergeUserFields(SMSBody, allUsers, replySMSNotificationSendTo) {
    var fieldFound, processKey;

    if (SMSBody != "") {
        for (var i = 0; i < allUsers.length; i++) {
            if (allUsers[i].id == replySMSNotificationSendTo) {
                $.each(allUsers[i], function(fkey, fval) {
                    fieldFound = SMSBody.indexOf(fkey);
                    if (fieldFound >= 0) {
                        processKey = "${Users." + fkey + "}";
                        SMSBody = SMSBody.replace(processKey, fval);
                    }
                });
            }
        }
    }
    return SMSBody;
}

function processOptions(fieldsWithLayout, sectionName, moduleName) {
    var moduleFields, fieldApiName, fieldDisplayName;
    var fieldListed = new Array();
    var mNameSingular = moduleName.substr(0, moduleName.length - 1);
    var procSecName = mNameSingular + " Information";

    if (sectionName == procSecName) {
        moduleFields =
            moduleFields +
            '<option value="${' +
            moduleName +
            '.id}">Record Id</option>';
    }

    if (
        (moduleName == "Events" || moduleName == "Campaigns") &&
        (sectionName == "Event Information" ||
            sectionName == "Campaign Information")
    ) {
        moduleFields =
            moduleFields +
            '<option value="${' +
            moduleName +
            '.First_Name}">Receiver First Name</option>';
        moduleFields =
            moduleFields +
            '<option value="${' +
            moduleName +
            '.Last_Name}">Receiver Last Name</option>';
    }

    if (
        moduleName == "Deals" &&
        (sectionName == "Deal Information" ||
            sectionName == "Potential Information")
    ) {
        moduleFields =
            moduleFields +
            '<option value="${' +
            moduleName +
            '.Contact_First_Name}">Contact First Name</option>';
        moduleFields =
            moduleFields +
            '<option value="${' +
            moduleName +
            '.Contact_Last_Name}">Contact Last Name</option>';
    }

    $.each(fieldsWithLayout, function(layoutKey, layoutInfo) {
        $.each(layoutInfo.sections, function(section_key, sectionInfo) {
            if (sectionInfo.fields.length > 0) {
                currentSectionName = sectionInfo.name;
                if (sectionName == currentSectionName) {
                    $.each(sectionInfo.fields, function(fieldKey, fieldInfo) {
                        fieldApiName = fieldInfo.api_name;
                        fieldDisplayName = fieldInfo.field_label;
                        checkIndexOf = fieldListed.indexOf(fieldApiName);
                        if (checkIndexOf == "-1") {
                            if (moduleName == "Events") {
                                if (
                                    fieldApiName != "Participants" &&
                                    fieldApiName != "Who_Id" &&
                                    fieldApiName != "Who_Id" &&
                                    fieldApiName != "What_Id"
                                ) {
                                    moduleFields =
                                        moduleFields +
                                        '<option value="${' +
                                        moduleName +
                                        "." +
                                        fieldApiName +
                                        '}">' +
                                        fieldDisplayName +
                                        "</option>";
                                    fieldListed.push(fieldApiName);
                                }
                            } else {
                                moduleFields =
                                    moduleFields +
                                    '<option value="${' +
                                    moduleName +
                                    "." +
                                    fieldApiName +
                                    '}">' +
                                    fieldDisplayName +
                                    "</option>";
                                fieldListed.push(fieldApiName);
                            }
                        }
                    });
                }
            }
        });
    });
    return moduleFields;
}

function mergeRecordFields(SMSBody, recordInfo, mobuleName, CRMTimeZone) {
    var fieldFound,
        processKey,
        processdate,
        pDay,
        pMonth,
        pYear,
        pHour,
        pMinute,
        pAMPM;
    if (SMSBody != "") {
        $.each(recordInfo, function(fkey, fval) {
            fieldFound = SMSBody.indexOf(fkey);
            if (fieldFound >= 0) {
                processKey = "${" + mobuleName + "." + fkey + "}";
                //process if there is subobj

                if (
                    fkey == "Created_By" ||
                    fkey == "Modified_By" ||
                    fkey == "Owner" ||
                    (fkey == "Contact_Name" && mobuleName == "Deals") ||
                    (fkey == "Account_Name" && mobuleName == "Deals")
                ) {
                    if (fval != null) {
                        fval = fval.name;
                    }
                } else if (fkey == "Modified_Time" || fkey == "Created_Time") {
                    var newDateAccUserTZ = moment(fval).tz(CRMTimeZone);
                    fval = newDateAccUserTZ.format("DD/MM/YYYY hh:mm A");
                } else if (fkey == "Closing_Date") {
                    processdate = new Date(fval);
                    pDay = addZero(processdate.getDate());
                    pMonth = addZero(processdate.getMonth());
                    pYear = processdate.getFullYear();
                    fval = pDay + "/" + pMonth + "/" + pYear;
                }

                //process if there is null value
                if (fval == "null" || fval == null) {
                    fval = "";
                }

                //Check if the Value contain date time
                var dateTimeVal = checkIfDateTimeString(fval);
                if (dateTimeVal) {
                    var newDateAccUserTZ = moment(fval).tz(CRMTimeZone);
                    fval = newDateAccUserTZ.format("DD/MM/YYYY hh:mm A");
                }

                SMSBody = SMSBody.replace(processKey, fval);
            }
        });
    }
    return SMSBody;
}

//Filter specific templates for particular module
function filterTemplates(templateInfo, moduleName) {
    var returnFalg = false;
    $.each(templateInfo, function(fieldName, fieldValue) {
        if (
            fieldName == "oscsmsglobal__Module_Name" &&
            fieldValue == moduleName
        ) {
            returnFalg = true;
        }
    });

    return returnFalg;
}
function processHour(ptbHour) {
    if (ptbHour > 12) {
        ptbHour = ptbHour - 12;
    }
    ptbHour = addZero(ptbHour);
    return ptbHour;
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function decideAMORPM(oHour) {
    var pAmPm = "AM";
    if (oHour >= 12) {
        pAmPm = "PM";
    }
    return pAmPm;
}

var factor = 750;

function setDeceleratingTimeout(callback, times) {
    var internalCallback = (function(tick, counter) {
        return function() {
            if (--tick >= 0) {
                window.setTimeout(internalCallback, factor);
                callback();
            }
        };
    })(times, 0);

    window.setTimeout(internalCallback, factor);
}

function populateTimePartOptions() {
    var scheduleTimeOptions;
    for (var i = 0; i <= 23; i++) {
        var hourPart = processHour(i);
        hourPart = hourPart == "00" ? "12" : hourPart;
        var hoursSuffix = decideAMORPM(i);
        scheduleTimeOptions =
            scheduleTimeOptions +
            '<option value="' +
            hourPart +
            ":00 " +
            hoursSuffix +
            '">' +
            hourPart +
            ":00 " +
            hoursSuffix +
            "</option>";
        scheduleTimeOptions =
            scheduleTimeOptions +
            '<option value="' +
            hourPart +
            ":30 " +
            hoursSuffix +
            '">' +
            hourPart +
            ":30 " +
            hoursSuffix +
            "</option>";
    }
    return scheduleTimeOptions;
}

function checkIfDateTimeString(stringVal) {
    var re = new RegExp(/\b\d+-\d+-\d+T\d+:\d+:\d+\b/);
    var flag = re.exec(stringVal);

    if (flag == null) {
        return false;
    } else {
        return true;
        //var s = "Match at position " + m.index + ":\n";
        //for (i = 0; i < m.length; i++) {
        //s = s + m[i] + "\n";
        //}
        //alert(s);
    }
}
